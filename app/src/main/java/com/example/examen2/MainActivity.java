package com.example.examen2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import database.AccionesProducto;
import database.Producto;

public class MainActivity extends AppCompatActivity {
    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup grupo;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnNuevo;
    private Button btnEditar;
    private AccionesProducto db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtCodigo = (EditText) findViewById(R.id.txtCodigo);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtMarca = (EditText) findViewById(R.id.txtMarca);
        txtPrecio = (EditText) findViewById(R.id.txtPrecio);
        grupo = (RadioGroup) findViewById(R.id.grpProductos);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnNuevo = (Button) findViewById(R.id.btnNuevo);
        btnEditar = (Button) findViewById(R.id.btnEditar);

        db = new AccionesProducto(this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtCodigo.getText().toString().matches("") || txtNombre.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Favor de llenar al menos el código y nombre", Toast.LENGTH_SHORT).show();
                }
                else {
                    // Verifica si ya existe
                    if(db.consultaCodigoProd(Long.parseLong(txtCodigo.getText().toString())) != null){
                        Toast.makeText(MainActivity.this, "el producto ya existe", Toast.LENGTH_SHORT).show();
                    }
                    //Si no existe agrega
                    else {
                        Producto prod = new Producto();
                        prod.setCodigo(Integer.parseInt(txtCodigo.getText().toString()));
                        prod.setNombre(txtNombre.getText().toString());
                        prod.setMarca(txtMarca.getText().toString());
                        prod.setPrecio(Float.parseFloat(txtPrecio.getText().toString()));
                        prod.setPerecedero(grupo.getCheckedRadioButtonId() == R.id.rbPerecedero ? 1 : 0);
                        db.openDatabase();
                        if (db.insertarProducto(prod) > 0) {
                            Toast.makeText(MainActivity.this, "Se agregó el producto con código: " + prod.getCodigo(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, "Ocurrió un error...", Toast.LENGTH_SHORT).show();
                        }
                        limpiar();
                    }
                    db.cerrar();
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Limpia los campos para agregar un nuevo producto aquí mismo
                limpiar();
            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // cargar ProductoActivity con los datos limpios
                Intent intent = new Intent(MainActivity.this, ProductoActivity.class);
                startActivity(intent);
            }
        });

    } //fin onCreate

    public void limpiar() {
        txtCodigo.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        txtPrecio.setText("");
        RadioButton primero = (RadioButton) grupo.getChildAt(0);
        primero.setChecked(true);
    }
}
