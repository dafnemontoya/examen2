package com.example.examen2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import database.AccionesProducto;
import database.Producto;

public class ProductoActivity extends AppCompatActivity {
    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup grupo;
    private Button btnBuscar;
    private Button btnBorrar;
    private Button btnActualizar;
    private Button btnCerrar;
    private Producto prodGuardado = null;
    private AccionesProducto db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);

        txtCodigo = (EditText) findViewById(R.id.txtCodigo);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtMarca = (EditText) findViewById(R.id.txtMarca);
        txtPrecio = (EditText) findViewById(R.id.txtPrecio);
        grupo = (RadioGroup) findViewById(R.id.grpProductos);
        btnBuscar = (Button) findViewById(R.id.btnBuscar);
        btnBorrar = (Button) findViewById(R.id.btnBorrar);
        btnActualizar = (Button) findViewById(R.id.btnActualizar);
        btnCerrar = (Button) findViewById(R.id.btnSalir);

        db = new AccionesProducto(this);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDatabase();
                Producto prod = db.consultaCodigoProd(Long.parseLong(txtCodigo.getText().toString()));;

                if (prod == null) {
                    Toast.makeText(ProductoActivity.this, "No existe el registro", Toast.LENGTH_SHORT).show();
                }
                else {
                    prodGuardado = prod;
                    txtNombre.setText(prod.getNombre());
                    txtMarca.setText(prod.getMarca());
                    txtPrecio.setText(String.valueOf(prod.getPrecio()));
                    grupo.check(prodGuardado.getPerecedero() == 1 ? R.id.rbPerecedero : R.id.rbNoPerecedero);
                }
                db.cerrar();
            }
        });

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDatabase();
                db.eliminarProducto(prodGuardado.get_ID());
                Toast.makeText(ProductoActivity.this, "Se elimino el producto", Toast.LENGTH_LONG).show();
                db.cerrar();
                limpiar();
            }
        });

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDatabase();
                if (txtCodigo.getText().toString().matches("") || txtNombre.getText().toString().matches("")) {
                    Toast.makeText(ProductoActivity.this, "Favor de llenar al menos el código y nombre", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Producto prod = new Producto();
                    prod.set_ID(prodGuardado.get_ID());
                    prod.setCodigo(Integer.parseInt(txtCodigo.getText().toString()));
                    prod.setNombre(txtNombre.getText().toString());
                    prod.setMarca(txtMarca.getText().toString());
                    prod.setPrecio(Float.parseFloat(txtPrecio.getText().toString()));
                    prod.setPerecedero(grupo.getCheckedRadioButtonId() == R.id.rbPerecedero ? 1 : 0);

                    if (db.actualizarProducto(prod, prod.get_ID()) > 0)
                        Toast.makeText(ProductoActivity.this, "Se actualizó el producto con codigo "+ prod.getCodigo(), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(ProductoActivity.this, "Ocurrió un error...", Toast.LENGTH_SHORT).show();
                    limpiar();
                }

                db.cerrar();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void limpiar() {
        txtCodigo.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        txtPrecio.setText("");
        RadioButton primero = (RadioButton) grupo.getChildAt(0);
        primero.setChecked(true);
        prodGuardado = null;
    }
}
