package database;

import java.io.Serializable;

public class Producto implements Serializable {
    private long _ID;
    private int codigo;
    private String nombre;
    private String marca;
    private float precio;
    private int perecedero;

    public Producto() {
    }

    public Producto(long _ID, int codigo, String nombre, String marca, float precio, int perecedero) {
        this._ID = _ID;
        this.codigo = codigo;
        this.nombre = nombre;
        this.marca = marca;
        this.precio = precio;
        this.perecedero = perecedero;
    }

    public long get_ID() {
        return _ID;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getPerecedero() {
        return perecedero;
    }

    public void setPerecedero(int perecedero) {
        this.perecedero = perecedero;
    }
}
