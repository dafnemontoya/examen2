package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class ProductoDBHelper extends SQLiteOpenHelper {
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA = " ,";

    private static final String SQL_CREATE_PRODUCTO = " CREATE TABLE " +
            DefinirTabla.Producto.TABLE_NAME + " (" +
            DefinirTabla.Producto._ID + " INTEGER PRIMARY KEY, " +
            DefinirTabla.Producto.CODIGO + INTEGER_TYPE + COMMA +
            DefinirTabla.Producto.NOMBRE + TEXT_TYPE + COMMA +
            DefinirTabla.Producto.MARCA + TEXT_TYPE + COMMA +
            DefinirTabla.Producto.PRECIO + REAL_TYPE + COMMA +
            DefinirTabla.Producto.PERECEDERO + INTEGER_TYPE + ")";

    private static final String SQL_DELETE_PRODUCTO = "DROP TABLE IF EXISTS " +
            DefinirTabla.Producto.TABLE_NAME;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "sistema.db";

    //Constructor
    public ProductoDBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Métodos sobreescritos
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PRODUCTO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_PRODUCTO);
        onCreate(db);
    }
}
