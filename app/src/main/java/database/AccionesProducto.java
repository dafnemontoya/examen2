package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class AccionesProducto {
    private Context context;
    private ProductoDBHelper productoDBHelper;
    private SQLiteDatabase db;
    private String[] columnToRead = new String[] {
        DefinirTabla.Producto._ID,
        DefinirTabla.Producto.CODIGO,
        DefinirTabla.Producto.NOMBRE,
        DefinirTabla.Producto.MARCA,
        DefinirTabla.Producto.PRECIO,
        DefinirTabla.Producto.PERECEDERO,
    };

    public AccionesProducto(Context context) {
        this.context = context;
        this.productoDBHelper = new ProductoDBHelper(this.context);
    }

    public void openDatabase() {
        db = productoDBHelper.getWritableDatabase();
    }

    public long insertarProducto(Producto p) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.CODIGO, p.getCodigo());
        values.put(DefinirTabla.Producto.NOMBRE, p.getNombre());
        values.put(DefinirTabla.Producto.MARCA, p.getMarca());
        values.put(DefinirTabla.Producto.PRECIO, p.getPrecio());
        values.put(DefinirTabla.Producto.PERECEDERO, p.getPerecedero());

        return db.insert(DefinirTabla.Producto.TABLE_NAME, null, values);
    }

    public long actualizarProducto(Producto p, long id) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.CODIGO, p.getCodigo());
        values.put(DefinirTabla.Producto.NOMBRE, p.getNombre());
        values.put(DefinirTabla.Producto.MARCA, p.getMarca());
        values.put(DefinirTabla.Producto.PRECIO, p.getPrecio());
        values.put(DefinirTabla.Producto.PERECEDERO, p.getPerecedero());

        String criterio = DefinirTabla.Producto._ID + " = " + id;

        return db.update(DefinirTabla.Producto.TABLE_NAME, values, criterio, null);
    }

    public long eliminarProducto(long id) {
        String criterio = DefinirTabla.Producto._ID + " = " + id;
        return db.delete(DefinirTabla.Producto.TABLE_NAME, criterio, null);
    }

    public Producto leerProducto(Cursor cursor) {
        Producto p = new Producto();

        p.set_ID(cursor.getInt(0));
        p.setCodigo(cursor.getInt(1));
        p.setNombre(cursor.getString(2));
        p.setMarca(cursor.getString(3));
        p.setPrecio(cursor.getFloat(4));
        p.setPerecedero(cursor.getInt(5));

        return p;
    }

    public Producto getProducto(long id) {
        Producto producto = null;
        SQLiteDatabase db = this.productoDBHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Producto.TABLE_NAME, columnToRead,
                DefinirTabla.Producto._ID + " = ? ",
                new String[] { String.valueOf(id) },
                null, null, null);

        if (c.moveToFirst()) {
            producto = leerProducto(c);
        }
        c.close();
        return producto;
    }

    //Necesitaré extraer sólo el código del producto
    public Producto consultaCodigoProd (long codigo){
        Producto producto = null;
        SQLiteDatabase db = productoDBHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Producto.TABLE_NAME,
                columnToRead,
                DefinirTabla.Producto.CODIGO + " = ? ",
                new String[] {String.valueOf(codigo)},
                null, null,null);
        if(c.moveToFirst()){
            producto = leerProducto(c);
        }
        c.close();
        return producto;
    }


    public ArrayList<Producto> allProductos() {
        ArrayList<Producto> productos = new ArrayList<Producto>();
        Cursor cursor = db.query(DefinirTabla.Producto.TABLE_NAME,
                null, null, null,
                null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Producto p = leerProducto(cursor);
            productos.add(p);
            cursor.moveToNext();
        }

        cursor.close();
        return productos;
    }

    public void cerrar() {
        productoDBHelper.close();
    }

}
